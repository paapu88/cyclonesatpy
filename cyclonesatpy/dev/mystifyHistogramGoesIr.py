"""
https://stackoverflow.com/questions/57222616/find-function-minimum-for-discrete-variables-w-bounds-and-constraints

python mystifyHistogramGoesIr.py --method fmin --doDecrease False --doBounds False > fminFF.out &
python mystifyHistogramGoesIr.py --method fmin_powell --doDecrease False --doBounds False > fmin_powellFF.out &
python mystifyHistogramGoesIr.py --method diffev --doDecrease False --doBounds False > diffevFF.out &
python mystifyHistogramGoesIr.py --method diffev2 --doDecrease False --doBounds False > diffev2FF.out &

python mystifyHistogramGoesIr.py --method fmin --doDecrease True --doBounds True > fminTT.out &
python mystifyHistogramGoesIr.py --method fmin_powell --doDecrease True --doBounds True > fmin_powellTT.out &
python mystifyHistogramGoesIr.py --method diffev --doDecrease True --doBounds True > diffevTT.out &
python mystifyHistogramGoesIr.py --method diffev2 --doDecrease True --doBounds True > diffev2TT.out &

python mystifyHistogramGoesIr.py --method fmin --doDecrease True --doBounds False > fminTF.out &
python mystifyHistogramGoesIr.py --method fmin_powell --doDecrease True --doBounds False > fmin_powellTF.out &
python mystifyHistogramGoesIr.py --method diffev --doDecrease True --doBounds False > diffevTF.out &
python mystifyHistogramGoesIr.py --method diffev2 --doDecrease True --doBounds False > diffev2TF.out &
"""

from pathlib import Path
import os
import sys
import matplotlib.pyplot as plt
from pyproj import pj_list
import numpy as np
import seaborn as sns
from PIL import Image
from sklearn.neighbors import KernelDensity
import boto3
import random
from cyclonesatpy.timehelp import get_navymil_time
from cyclonesatpy.maphelp import get_navymil_lat_lon
from cyclonesatpy.iohelp import get_satellite, get_satellite_data, get_himawari_aws_fileNames, get_goes_aws_fileNames
from cyclonesatpy.mischelp import replace_nans, str2bool

from cyclonesatpy.maphelp import make_and_save_satpy_image_colorize_mapping
from sklearn.metrics import mean_squared_error

from mystic.symbolic import generate_penalty, generate_conditions
from mystic.constraints import integers
from mystic.solvers import diffev, diffev2, fmin_powell, fmin, lattice, buckshot
from mystic.monitors import VerboseMonitor
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--method", help="calculation method")
parser.add_argument(
    "--doDecrease",
    type=str2bool,
    nargs="?",
    const=True,
    default=True,
    help="Should we require a decreasing function?",
)
parser.add_argument(
    "--doBounds",
    type=str2bool,
    nargs="?",
    const=True,
    default=True,
    help="Should we require function to bounded 0..255?",
)
args = parser.parse_args()

@integers()
def round(x):
    return x

# take random navymil image
# below you get the path and name of all objects that have a given prefix (i.e. within a given path)
# you can then filter the elements of the list, or format the info in a different way, before you write it in the csv file.
my_bucket_name = "newnavy"
my_path = "tcdat/tc20/"
channelInit = "ir"
nFiles = 5
xsize = 1024
ysize = 1024
cut = 100
resolution = 2
proj = "merc"
myhome = os.path.join(Path.home(), "Downloads")
s3_resource = boto3.resource("s3")
my_bucket = s3_resource.Bucket(my_bucket_name)
my_list = []
icount = 0
s3 = boto3.resource("s3")

"""
for o in my_bucket.objects.filter(Prefix=my_path):
    if (
        ("hm8" in o.key or "goes" in o.key)
        and "ir/geo/1km_bw" in o.key
        and o.key.endswith(".jpg")
    ):
        # if o.key.endswith('.jpg'):
        my_list.append(o.key)
        # print(f"i, file {icount, my_list[-1]}")
        icount += 1
        if icount % 5000 == 0:
            print(icount, o.key)
        if icount > 5000:
            break

filepaths = random.sample(my_list, nFiles)
"""
filepaths = ['tcdat/tc20/ATL/31L.IOTA/ir/geo/1km_bw/20201116.1800.goes17.x.ir1km_bw.31LIOTA.140kts-918mb-135N-823W.100pc.jpg']
print(filepaths)
imgs = []
imgsNavy = []
# histogram for navymil images
for filepath in filepaths:
    localFile = os.path.join(myhome, filepath.split("/")[-1])
    my_bucket.download_file(filepath, localFile)

    """
    filepath = (
        "20200605.0330.goes17.x.ir1km_bw.03LCRISTOBAL.30kts-1000mb-183N-902W.100pc.jpg"
    )
    localFile = os.path.join(myhome, filepath)
    """

    print(f"Navymil file: {localFile}")

    img = Image.open(localFile)
    w, h = img.size
    left = 0
    upper = cut
    right = w
    lower = h - cut

    area = (left, upper, right, lower)
    img = img.crop(area)
    imgsNavy.append(img)
    # img.show()
    img = np.array(img)
    img = replace_nans(img)
    imgs.append(img)
    # histograms
navy = np.array(imgs)
navyhist = np.histogram(navy, density=True, bins=256, range=(0, 256))
# remove black and white (0 and 255 that are in lines)
# print(f"before: {navyhist[0]}")
# navyhist[0][0] = navyhist[0][1]
# navyhist[0][255] = navyhist[0][254]
# print(f"after: {navyhist[0]}")
# print(f"after: {navyhist[0].shape}")


# plt.plot(navyhist[0], label="Navymil")
# plt.legend(loc="upper left")
# plt.show()

# download satellite raw data, get the names of loaded files and resulting local images (not yet making images)
outFileNames = []
localFileNames = []
lats = []
lons = []
readers = []
channels = []
reader_kwargss = []

for filepath in filepaths:
    localFile = os.path.join(myhome, filepath.split("/")[-1])
    datetimeCurrent = get_navymil_time(filename=localFile.split("/")[-1])
    lat, lon = get_navymil_lat_lon(filename=localFile.split("/")[-1])
    print(f"time, lat, lon: {datetimeCurrent, lat, lon}")
    #lon=-80.3 # hack to get the same area as navymil... for iota 2020

    # take our image from the same time and area
    satellite = get_satellite(lon0=lon)
    if satellite is None:
        continue
    (
        bucket,
        product,
        channel,
        reader,
        reader_kwargs,
        numberOfFiles,
    ) = get_satellite_data(satellite=satellite, channelInit=channelInit)
    print("calling")
    if satellite == "himawari":
        awsfilenames = get_himawari_aws_fileNames(
            currentDate=datetimeCurrent,
            numberOfFiles=numberOfFiles,
            product=product,
            bucket=bucket,
            channel=channel,
        )
    elif satellite in ["goes16", "goes17"]:
        awsfilenames = get_goes_aws_fileNames(
            currentDate=datetimeCurrent,
            product=product,
            bucket=bucket,
            channel=channel,
        )
    else:
        print(f"only satelites 'himawari', 'goes16', 'goes17' implemented")

    myBucket = s3.Bucket(bucket)
    localFiles = []
    for awsfilename in awsfilenames:
        localFile = os.path.join(myhome, awsfilename.split("/")[-1])
        print("localfile,", localFile)
        print("aws file: ", awsfilename)
        if Path(os.path.join(localFile)).is_file():
            print(f"file {localFile} exists, not downloading")
            localFiles.append(localFile)
            continue
        else:
            print(f"Downloading  {localFile}")
        myBucket.download_file(
            awsfilename,
            localFile,
        )
        localFiles.append(localFile)
    outFileName = (
        str(datetimeCurrent)
        + satellite
        + "_"
        + "lat"
        + str(np.around(float(lat), 4))
        + "_"
        + "lon"
        + str(np.around(float(lon), 4))
        + ".jpg"
    )
    outFileName = outFileName.replace(" ", "-").replace(":", "-")
    print(f"out:{outFileName}")
    outFileNames.append(outFileName)
    localFileNames.append(localFiles)
    lats.append(lat)
    lons.append(lon)
    readers.append(reader)
    channels.append(channel)
    reader_kwargss.append(reader_kwargs)


def cost(x):
    global best
    global myname
    ours = []
    imgsOur=[]
    for outFileName, localFiles, lat, lon, reader, channel, reader_kwargs in zip(
        outFileNames, localFileNames, lats, lons, readers, channels, reader_kwargss
    ):
        # build the image using satpy
        ourfilename, imgOur, dummy, dummy = make_and_save_satpy_image_colorize_mapping(
            localFiles=localFiles,
            lat_0=lat,
            lon_0=lon,
            xsize=xsize,
            ysize=ysize,
            resolution=resolution,
            proj=proj,
            reader=reader,
            reader_kwargs=reader_kwargs,
            channel=channel,
            outDir=myhome,
            outFileName=outFileName,
            shift=-273.15,
            x0=np.array(x),
            showCoast=True,
            coastColor=(255, 0, 0),
            showLatLonGrid=True,
            gridColor="white",
        )

        #imgOur = Image.fromarray(imgOur)
        imgOur = imgOur.crop(area)
        imgsOur.append(imgOur)
        #if random.random() > 0.99:
        #    imgOur.show()
        ours.append(np.array(imgOur))
    ours = np.array(ours)
    ours = replace_nans(ours)
    #print(f"Min max of data {np.nanmin(ours), np.nanmax(ours)}")
    ourhist = np.histogram(ours, density=True, bins=256, range=(0, 256))
    #print(f"x {x[0:10], x[-10:]}")
    penalty = mean_squared_error(navyhist[0], ourhist[0])

    if best is None or penalty < best:
        print(f"penalty, best: {myname, penalty, best}, flush=True")
        sys.stdout.flush()
        best = penalty
        plt.plot(navyhist[0], label="Navymil")
        plt.plot(ourhist[0], label="Satpy " )
        plt.legend(loc="upper left")
        plt.savefig(str(np.around(mean_squared_error(navyhist[0], ourhist[0])*1,1)) + myname+ "_hist_test.jpg")
        plt.clf()
        print(f"x: {x}")
        plt.plot(x, label="mapping")
        plt.legend(loc="upper right")
        plt.savefig(str(np.around(mean_squared_error(navyhist[0], ourhist[0])*1,1)) + myname+ "_mapping_test.jpg")
        plt.clf()
        for i, (our, navy) in enumerate(zip(imgsOur, imgsNavy)):
            # left, upper, right, and lower
            print(our.size, navy.size)
            result = Image.new("RGB", (1024 * 2, 824))
            result.paste(navy, box=(0, 0, 1024, 824))
            result.paste(our, (1024, 0, 2048, 824))
            result.save("pasted_last_"+str(i)+myname+'_compare_test.jpg')
    return mean_squared_error(navyhist[0], ourhist[0])


ticks = 110*2
x0=np.linspace(255,35, ticks).astype(np.dtype(np.uint16)) # color mapping 255...0 as integers
bounds = [(0,255)]*ticks
best = None


equations = ''
for p in range(1,ticks):
    equations += f'x{p} >= x{p-1}\n'
print(equations)
penaltyOfDecrease = generate_penalty(generate_conditions(equations))

if not args.doDecrease:
    penaltyOfDecrease=None
if not args.doBounds:
    bounds=None
myname = args.method + '_doDecrease'+str(args.doDecrease) + '_doBounds'+str(args.doBounds)

if args.method == 'fmin':
    result = fmin(cost, x0=x0, bounds=bounds, penalty=penaltyOfDecrease, constraints=round, full_output=True, itermon=VerboseMonitor())
elif args.method == 'fmin_powell':
    result = fmin_powell(cost, x0=x0, bounds=bounds, penalty=penaltyOfDecrease, constraints=round, full_output=True,
                  itermon=VerboseMonitor())
elif args.method == 'diffev':
    result = diffev(cost, npop=256, x0=x0, bounds=bounds, penalty=penaltyOfDecrease, constraints=round, full_output=True,
                  itermon=VerboseMonitor())
elif args.method == 'diffev2':
    result = diffev2(cost, npop=256, x0=x0, bounds=bounds, penalty=penaltyOfDecrease, constraints=round, full_output=True,
                  itermon=VerboseMonitor())
else:
    print(f"method {args.method} not implemented, stopping...")
    sys.exit()

# result = lattice(penalty, ndim=256, bounds=bounds, penalty=pf)
#result = buckshot(penalty, npts=12, ndim=256, bounds=bounds, penalty=pf)

print(result, flush=True)
#--------------------------------------------------------------------------------------------

# show final results
ours = []
imgsOur = []
for outFileName, localFiles, lat, lon, reader, channel, reader_kwargs in zip(
    outFileNames, localFileNames, lats, lons, readers, channels, reader_kwargss
):
    # build the image using satpy
    ourfilename, imgOur, dummy, dummy = make_and_save_satpy_image_colorize_mapping(
        localFiles=localFiles,
        lat_0=lat,
        lon_0=lon,
        xsize=xsize,
        ysize=ysize,
        resolution=resolution,
        proj=proj,
        reader=reader,
        reader_kwargs=reader_kwargs,
        channel=channel,
        outDir=myhome,
        outFileName=outFileName,
        shift=-273.15,
        x0=x0,
        showCoast=True,
        coastColor=(255, 0, 0),
        showLatLonGrid=True,
        gridColor="white",
    )

    #imgOur = Image.open(ourfilename)
    imgOur = imgOur.crop(area)
    imgsOur.append(imgOur)

    ours.append(np.array(imgOur))
ours = np.array(ours)
#datahist = np.histogram(datas, density=True, bins=110, range=(-80, 30))

ourhist = np.histogram(ours, density=True, bins=256, range=(0, 256))


print(f"penalty {mean_squared_error(navyhist[0], ourhist[0])}")
plt.plot(navyhist[0], label="Navymil")
plt.plot(ourhist[0], label="Satpy")
plt.legend(loc="upper left")
plt.show()
for our, navy in zip(imgsOur, imgsNavy):
    # left, upper, right, and lower
    print(our.size, navy.size)
    result = Image.new("RGB", (1024 * 2, 824))
    result.paste(navy, box=(0, 0, 1024, 824))
    result.paste(our, (1024, 0, 2048, 824))
    result.save("last_compare.jpg")
    result.show()
