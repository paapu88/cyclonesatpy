from pathlib import Path
import os
from cyclonesatpy.maphelp import (
    make_and_save_satpy_image_colorize_manual,
)
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import boto3
import random
from cyclonesatpy.timehelp import get_navymil_time

from cyclonesatpy.maphelp import get_navymil_lat_lon

from cyclonesatpy.iohelp import (
    get_satellite,
    get_satellite_data,
    get_himawari_aws_fileNames,
    get_goes_aws_fileNames,
)

from cyclonesatpy.mischelp import replace_nans

colorParameters = {"minGray": -90.0, "maxGray": 30.0}

# take random navymil image
# below you get the path and name of all objects that have a given prefix (i.e. within a given path)
# you can then filter the elements of the list, or format the info in a different way, before you write it in the csv file.
my_bucket_name = "newnavy"
my_path = "tcdat/tc20/"
channelInit = "ir"
nFiles = 5
xsize = 1024
ysize = 1024
cut = 100
resolution = 2
proj = "merc"
myhome = os.path.join(Path.home(), "Downloads")
s3_resource = boto3.resource("s3")
my_bucket = s3_resource.Bucket(my_bucket_name)
my_list = []
icount = 0
s3 = boto3.resource("s3")
debug = True
if debug:
    my_list = os.listdir(myhome)
    my_list = [f for f in my_list if f.endswith("100pc.jpg")]
    my_list = [f for f in my_list if f.startswith("2020")]
    # my_list = [
    #    "/home/hu-mka/Downloads/20200301.2000.hm8.x.ir1km_bw.19PESTHER.25kts-1004mb-182S-1257E.100pc.jpg"
    # ]
else:
    for o in my_bucket.objects.filter(Prefix=my_path):
        if (
            ("hm8" in o.key or "goes" in o.key)
            # ("goes" in o.key)
            and "ir/geo/1km_bw" in o.key
            and o.key.endswith(".jpg")
        ):
            # if o.key.endswith('.jpg'):
            my_list.append(o.key)
            # print(f"i, file {icount, my_list[-1]}")
            icount += 1
            if icount % 5000 == 0:
                print(icount, o.key)
filepaths = random.sample(my_list, nFiles)
print(f"filepaths: {filepaths}")

# filepaths = [
#    "/20201003.2030.goes16.x.ir1km_bw.18EMARIE.110kts-952mb-188N-1271W.100pc.jpg",
#    "/20201005.0330.goes16.x.ir1km_bw.18EMARIE.55kts-993mb-206N-1307W.100pc.jpg",
#    "/20200816.1800.goes16.x.ir1km_bw.10ETEN.30kts-1006mb-136N-1339W.100pc.jpg",
# ]

imgs = []
imgsNavy = []
# histogram for navymil images
for filepath in filepaths:
    localFile = os.path.join(myhome, filepath.split("/")[-1])
    if not debug:
        my_bucket.download_file(filepath, localFile)

    print(f"Navymil file: {localFile, debug}")

    img = Image.open(localFile)
    w, h = img.size
    left = 0
    upper = cut
    right = w
    lower = h - cut
    area = (left, upper, right, lower)
    img = img.crop(area)

    imgsNavy.append(img)
    # img.show()
    img = np.array(img)
    try:
        img = replace_nans(img)
    except:
        print("warning, could not remove nans from navymil...")
    imgs.append(img)
    # histograms
navy = np.array(imgs)
navyhist = np.histogram(navy, density=True, bins=256, range=(0, 256))

# download satellite raw data, get the names of loaded files and resulting local images (not yet making images)
outFileNames = []
localFileNames = []
lats = []
lons = []
readers = []
channels = []
reader_kwargss = []

for filepath in filepaths:
    localFile = os.path.join(myhome, filepath.split("/")[-1])
    datetimeCurrent = get_navymil_time(filename=localFile.split("/")[-1])
    lat, lon = get_navymil_lat_lon(filename=localFile.split("/")[-1])
    print(f"time, lat, lon: {datetimeCurrent, lat, lon}")

    # take our image from the same time and area
    satellite = get_satellite(lon0=lon)
    if satellite is None:
        continue
    (
        bucket,
        product,
        channel,
        reader,
        reader_kwargs,
        numberOfFiles,
    ) = get_satellite_data(satellite=satellite, channelInit=channelInit)
    print("calling")
    if satellite == "himawari":
        awsfilenames = get_himawari_aws_fileNames(
            currentDate=datetimeCurrent,
            numberOfFiles=numberOfFiles,
            product=product,
            bucket=bucket,
            channel=channel,
        )
    elif satellite in ["goes16", "goes17"]:
        awsfilenames = get_goes_aws_fileNames(
            currentDate=datetimeCurrent,
            product=product,
            bucket=bucket,
            channel=channel,
        )
    else:
        print(f"only satelites 'himawari', 'goes16', 'goes17' implemented")

    myBucket = s3.Bucket(bucket)
    localFiles = []
    for awsfilename in awsfilenames:
        localFile = os.path.join(myhome, awsfilename.split("/")[-1])
        print("localfile,", localFile)
        print("aws file: ", awsfilename)
        if Path(os.path.join(localFile)).is_file():
            print(f"file {localFile} exists, not downloading")
            localFiles.append(localFile)
            continue
        else:
            print(f"Downloading  {localFile}")
        myBucket.download_file(
            awsfilename,
            localFile,
        )
        localFiles.append(localFile)
    outFileName = (
        str(datetimeCurrent)
        + satellite
        + "_"
        + "lat"
        + str(round(float(lat), 4))
        + "_"
        + "lon"
        + str(round(float(lon), 4))
        + ".jpg"
    )
    outFileName = outFileName.replace(" ", "-").replace(":", "-")
    print(f"out:{outFileName}")
    outFileNames.append(outFileName)
    localFileNames.append(localFiles)
    lats.append(lat)
    lons.append(lon)
    readers.append(reader)
    channels.append(channel)
    reader_kwargss.append(reader_kwargs)


ours = []
datas = []
imgsOur = []
for outFileName, localFiles, lat, lon, reader, channel, reader_kwargs in zip(
    outFileNames, localFileNames, lats, lons, readers, channels, reader_kwargss
):
    # build the image using satpy
    ourfilename, imgOur, npimg, data = make_and_save_satpy_image_colorize_manual(
        localFiles=localFiles,
        lat_0=lat,
        lon_0=lon,
        xsize=xsize,
        ysize=ysize,
        resolution=resolution,
        proj=proj,
        reader=reader,
        reader_kwargs=reader_kwargs,
        channel=channel,
        outDir=myhome,
        outFileName=outFileName,
        shift=-273.15,
        showCoast=True,
        coastColor=(241, 171, 0),
        showLatLonGrid=False,
        gridColor="white",
    )
    # imgOur = Image.open(ourfilename)
    imgOur = imgOur.crop(area)
    imgsOur.append((imgOur))
    ours.append(npimg)
    datas.append(data)
ours = np.array(ours)
datas = np.array(datas)
ourhist = np.histogram(ours, density=True, bins=256, range=(0, 256))
datahist = np.histogram(data, density=True, bins=110, range=(-80, 30))


plt.plot(datahist[0], label="Data")
plt.legend(loc="upper left")
plt.savefig("hist_data.jpg")
plt.show()
plt.clf()

plt.plot(navyhist[0], label="Navymil")
plt.plot(ourhist[0], label="Satpy")
plt.legend(loc="upper left")
plt.savefig("hist_navy_our.jpg")
plt.show()

for i, (our, navy) in enumerate(zip(imgsOur, imgsNavy)):
    # left, upper, right, and lower
    print(our.size, navy.size)
    result = Image.new("RGB", (1024 * 2, 824))
    result.paste(navy, box=(0, 0, 1024, 824))
    result.paste(our, (1024, 0, 2048, 824))
    result.save("last_compare_" + str(i) + ".jpg")

    result.show()
