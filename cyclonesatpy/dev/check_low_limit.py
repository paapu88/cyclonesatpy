"""
try tofigure out should lower limit be -80 or -90 C
"""
import os
from pathlib import Path
import boto3
from PIL import Image
import numpy as np
from cyclonesatpy.timehelp import get_navymil_time
from cyclonesatpy.maphelp import get_navymil_lat_lon
from cyclonesatpy.iohelp import get_satellite, get_satellite_data, get_himawari_aws_fileNames, get_goes_aws_fileNames
from cyclonesatpy.mischelp import replace_nans, str2bool
from cyclonesatpy.maphelp import make_and_save_satpy_image_colorize_mapping

channelInit = "ir"
xsize = 1024
ysize = 1024
cut = 100
resolution = 2
proj = "merc"
ticks = 256*1
x0=np.linspace(255,0, ticks).astype(np.dtype(np.uint16)) # color mapping 255...0 as integers
shift = -273.15
my_bucket_name = "newnavy"
s3 = boto3.resource("s3")
my_bucket = s3.Bucket(my_bucket_name)

myhome = os.path.join(Path.home(), "Downloads")
filepaths = ['tcdat/tc20/ATL/31L.IOTA/ir/geo/1km_bw/20201116.1800.goes17.x.ir1km_bw.31LIOTA.140kts-918mb-135N-823W.100pc.jpg']
for filepath in filepaths:
    localFile = os.path.join(myhome, filepath.split("/")[-1])
    my_bucket.download_file(filepath, localFile)

    img = Image.open(localFile).convert('L')
    w, h = img.size
    left = 0
    upper = cut
    right = w
    lower = h - cut

    area = (left, upper, right, lower)
    #img = img.crop(area)

    imgnp = np.array(img)
    img.show()
    print(f"max {np.max(imgnp)}")
    print(f"min {np.min(imgnp)}")

    print(imgnp.shape)
    #mask = imgnp>254
    mask = imgnp<60
    imgnp[mask] = 0
    imgnp[~mask] = 255

    img2 = Image.fromarray(imgnp)
    #img2.show()

    datetimeCurrent = get_navymil_time(filename=localFile.split("/")[-1])
    lat, lon = get_navymil_lat_lon(filename=localFile.split("/")[-1])
    print(f"time, lat, lon: {datetimeCurrent, lat, lon}")
    #lon=-80.3 # hack to get the same area as navymil... for iota 2020

    # take our image from the same time and area
    satellite = get_satellite(lon0=lon)
    (
        bucket,
        product,
        channel,
        reader,
        reader_kwargs,
        numberOfFiles,
    ) = get_satellite_data(satellite=satellite, channelInit=channelInit)

    print("calling")
    if satellite == "himawari":
        awsfilenames = get_himawari_aws_fileNames(
            currentDate=datetimeCurrent,
            numberOfFiles=numberOfFiles,
            product=product,
            bucket=bucket,
            channel=channel,
        )
    elif satellite in ["goes16", "goes17"]:
        awsfilenames = get_goes_aws_fileNames(
            currentDate=datetimeCurrent,
            product=product,
            bucket=bucket,
            channel=channel,
        )
    else:
        print(f"only satelites 'himawari', 'goes16', 'goes17' implemented")

    myBucket = s3.Bucket(bucket)
    localFiles = []
    for awsfilename in awsfilenames:
        localFile = os.path.join(myhome, awsfilename.split("/")[-1])
        print("localfile,", localFile)
        print("aws file: ", awsfilename)
        if Path(os.path.join(localFile)).is_file():
            print(f"file {localFile} exists, not downloading")
            localFiles.append(localFile)
            continue
        else:
            print(f"Downloading  {localFile}")
        myBucket.download_file(
            awsfilename,
            localFile,
        )
        localFiles.append(localFile)
    outFileName = (
        str(datetimeCurrent)
        + satellite
        + "_"
        + "lat"
        + str(np.around(float(lat), 4))
        + "_"
        + "lon"
        + str(np.around(float(lon), 4))
        + ".jpg"
    )
    outFileName = outFileName.replace(" ", "-").replace(":", "-")

    ourfilename, imgOur, npimg, data = make_and_save_satpy_image_colorize_mapping(
    localFiles=[localFile],
    lat_0=lat,
    lon_0=lon,
    xsize=xsize,
    ysize=ysize,
    resolution=resolution,
    proj=proj,
    reader=reader,
    reader_kwargs=reader_kwargs,
    channel=channel,
    outDir=myhome,
    outFileName=outFileName,
    shift=shift,
    x0=x0,
    showCoast=True,
    coastColor=(255, 0, 0),
    showLatLonGrid=True,
    gridColor="white")

    imgOur.show()

    print(f"data min {np.min(data)}")
    print(f"data max {np.max(data)}")
    print(f"our npimg {npimg.shape}")
    #mask = data < -80.0
    mask = data > 29.0

    npimg[mask] = 0
    npimg[~mask] = 255
    img3 = Image.fromarray(npimg)
    #img3.show()