# read in copy pasted image from navymil colorscale
# get average valu
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

img = Image.open("./navymilIrScale.jpg")
img = img.resize((256, 5))
img = np.array(img)
print(img.shape)
img = np.average(img, axis=0)
print(img.shape)
img = np.average(img, axis=1)
print(img.shape)
img[0] = 255
img[1] = 254
img[2] = 253
img[255] = 0

img = np.round(img).astype(int)
print(img)
values = np.linspace(-80.0, 30.0, 256)
print(values.shape)
plt.scatter(x=range(len(img)), y=img)
plt.show()
