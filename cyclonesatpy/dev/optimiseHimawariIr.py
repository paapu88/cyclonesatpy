from pathlib import Path
import os
from cyclonesatpy.utlis import make_and_save_satpy_image
import matplotlib.pyplot as plt
from pyproj import pj_list

#export PYTHONPATH="$HOME/git/cyclonesatpy/"

localFiles = [
    "HS_H08_20200827_1210_B13_FLDK_R20_S0110.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0210.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0310.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0410.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0510.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0610.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0710.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0810.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S0910.DAT.bz2",
    "HS_H08_20200827_1210_B13_FLDK_R20_S1010.DAT.bz2",
]
lat=42.0
lon=125.0
xsize=1024
ysize=1024
resolution=2
proj='gall'
bucket = "noaa-himawari8"
numberOfFiles = 10
reader="ahi_hsd"
reader_kwargs = {"mask_space": False}
channel='B13'
outDir=os.path.join(Path.home(), 'git/cyclonesatpy/cyclonesatpy/dev')
outFileName=proj+'_BaviSatpyIr.png'

print(outFileName)
img = make_and_save_satpy_image(
                localFiles=localFiles,
                lat_0=lat,
                lon_0=lon,
                xsize=xsize,
                ysize=ysize,
                resolution=resolution,
                proj=proj,
                reader=reader,
                reader_kwargs=reader_kwargs,
                channel=channel,
                outDir=outDir,
                outFileName=outFileName,
            )

img.save(outFileName)
