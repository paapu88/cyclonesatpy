from pathlib import Path
import os
from cyclonesatpy.utlis import make_and_save_satpy_image
import matplotlib.pyplot as plt
from pyproj import pj_list

#export PYTHONPATH="$HOME/git/cyclonesatpy/"

localFiles = '/home/hu-mka/Downloads/OR_ABI-L1b-RadF-M6C13_G16_s20202992000196_e20202992009516_c20202992009585.nc'
lat=45.0
lon=-47.0
xsize=1024
ysize=1024
resolution=2
proj='merc'

reader="abi_l1b"
reader_kwargs = {}
channel='C13'
outDir=os.path.join(Path.home(), 'git/cyclonesatpy/cyclonesatpy/dev')
outFileName=proj+'etaSatpyIr.png'

print("ok 0")
outFileName = proj + 'etaSatpyIr.png'
print(outFileName)
img = make_and_save_satpy_image(
                localFiles=[localFiles],
                lat_0=lat,
                lon_0=lon,
                xsize=xsize,
                ysize=ysize,
                resolution=resolution,
                proj=proj,
                reader=reader,
                reader_kwargs=reader_kwargs,
                channel=channel,
                outDir=outDir,
                outFileName=outFileName,
            )

img.save(outFileName)
