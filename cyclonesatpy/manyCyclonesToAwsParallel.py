"""
from cyclone's lat lon as a function of time,
download the corresponding images
and also make a csv file in aws


# using default values from args and configs/configVal2020.json
#  storm list is given near the end of this file...

python manyCyclonesToAwsParallel.py

"""

from cyclonesatpy.satpyCycloneToAws import SatpyCycloneToAws
import multiprocessing as mp
import logging

logger = mp.log_to_stderr(logging.DEBUG)


class ManyCyclonesToAwsParallel(SatpyCycloneToAws):
    def __init__(
        self,
        homeDir,
        inDir,
        outDir,
        stormId,
        dMinutes,
        datetimeInit=None,
        datetimeFinal=None,
        **kwargs,
    ):
        super().__init__(
            homeDir=homeDir,
            inDir=inDir,
            outDir=outDir,
            stormId=stormId,
            dMinutes=dMinutes,
            datetimeInit=datetimeInit,
            datetimeFinal=datetimeFinal,
            **kwargs,
        )


if __name__ == "__main__":
    from cyclonesatpy.mischelp import str2bool
    import argparse
    from multiprocessing import Pool
    from dateutil import parser as dparser

    parser = argparse.ArgumentParser()
    parser.add_argument("--stormId", help="Storm name, use official id")
    parser.add_argument(
        "--homeDir", help="upmost local reference directory", default="HOME"
    )
    parser.add_argument(
        "--inDir",
        help="local dir where to download from aws (with respect to self.homeDir)",
        default="Downloads",
    )
    parser.add_argument(
        "--outDir",
        help="local dir where to write data (with respect to self.homeDir)",
        default="Downloads",
    )

    parser.add_argument(
        "--dMinutes",
        help="time step between satellite images, in minutes",
        type=int,
        default=30,
    )
    parser.add_argument("--datetimeInit", help="init time to start recording", type=str)
    parser.add_argument(
        "--datetimeFinal", help="final time to stop recording", type=str
    )
    parser.add_argument(
        "--uploadAws",
        type=str2bool,
        nargs="?",
        const=True,
        default=True,
        help="Should we upload to to aws?",
    )

    parser.add_argument(
        "--outAwsBucket",
        help="aws upload uppermost directory",
        default="hu-vals",
    )
    parser.add_argument(
        "--showLatLonGrid",
        type=str2bool,
        nargs="?",
        const=False,
        default=False,
        help="Show latlon lines in the image",
    )

    parser.add_argument(
        "--outAwsDir",
        help="aws upload uppermost directory",
        default="val2020",
    )

    parser.add_argument(
        "--channel", help="Which channel to make image ir/vis/singleVis", default="ir"
    )
    parser.add_argument(
        "--xsize", help="output image x size in pixels", type=int, default=1024
    )
    parser.add_argument(
        "--ysize", help="output image y size in pixels", type=int, default=1024
    )
    parser.add_argument(
        "--resolution",
        help="resolution of the output image in km",
        default=2.0,
        type=float,
    )
    parser.add_argument(
        "--proj", help="output map projection", type=str, default="merc"
    )
    parser.add_argument(
        "--nProc",
        help="number of processes that can run parallel",
        type=int,
        default=8,
    )

    args = parser.parse_args()
    if args.datetimeInit is None:
        datetimeInit = None
    else:
        datetimeInit = dparser.parse(args.datetimeInit)
    if args.datetimeFinal is None:
        datetimeFinal = None
    else:
        datetimeFinal = dparser.parse(args.datetimeFinal)

    def do_one_storm(stormId):
        logger.info(f"Received {stormId}")
        cm = SatpyCycloneToAws(
            homeDir=args.homeDir,
            inDir=args.inDir,
            outDir=args.inDir,
            stormId=stormId,
            dMinutes=args.dMinutes,
            datetimeInit=datetimeInit,
            datetimeFinal=datetimeFinal,
        )
        logger.info(f"after cm {stormId}")
        cm.cyclone_to_aws(
            channelInit=args.channel,
            xsize=args.xsize,
            ysize=args.ysize,
            resolution=args.resolution,
            proj=args.proj,
            outAwsBucket=args.outAwsBucket,
            outAwsDir=args.outAwsDir,
            showLatLonGrid=args.showLatLonGrid,
            uploadAws=args.uploadAws,
        )
        logger.info(f"after cm.cyclone_to_aws {stormId}")

    """
    stormIds = (
        "al082020 al092020 al132020 al142020 al162020 al172020 al192020 al202020 al262020 al272020 al282020 al292020 al312020 "
        + "ep082020 ep092020 ep122020 ep182020 "
        + "wp012020 wp032020 wp072020 wp092020 wp102020 wp112020 wp152020 wp162020 wp192020 wp212020 wp222020 wp252020 "
        + "io012020 "
        + "sh042020 sh072020 sh082020 sh142020 sh152020 sh252020"
    )
    """
    stormIds = (
        "al092020 al262020 al272020 al282020 al292020 "
        + "ep092020 ep122020 ep182020 "
        + "wp012020 wp032020 wp072020 wp092020 wp102020 wp112020 wp152020 wp162020 wp192020 wp212020 wp222020 wp252020 "
        + "io012020 "
        + "sh042020 sh072020 sh082020 sh142020 sh152020 sh252020"
    )
    stormIds = stormIds.split()

    pool = Pool(args.nProc)
    pool.map(do_one_storm, stormIds)
    pool.close()

    """ not possible storms 2020 season  
        "io022020" lon too west for himawari
        "sh012020" to early for himawari, himawari aws s3 starts 9.dec.2019 ( this starts 11/2019)
        "sh022020" lon too west for himawari
        "sh032020" lon too west for himawari
        "sh052020" lon too west for himawari
        "sh162020" lon too west for himawari
        "sh222020" lon too west for himawari
        "sh242020" lon too west for himawari
    
        possible ones with comments
    "io012020" very west for himawari8, but doable
    "sh042020" starts himawari8, then goes west (goes 17)
    "sh082020" starts himawari8, then goes west (goes 17)
    "sh252020" starts himawari8, then goes west (goes 17)

    """
