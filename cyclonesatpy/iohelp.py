"""
net routines
"""

import requests
import pandas as pd
import datetime
import s3fs
import boto3
from datetime import datetime, timedelta
from pathlib import Path
from botocore.exceptions import ClientError
import sys

from cyclonesatpy.timehelp import shifted_date_to_daynumber, round_to_x_mins


def select_satellite(lon0):
    """
    based on longitude, select best satellite that sees around that longitude

    Parameters
    ----------
    lon0 : float, longitude at which we want the data

    Returns
    -------
    string describing which aws3 bucket has the data
    or None if no satellite sees the desired longitude

    """
    if lon0 > 180.0:  # col state gives lons sometimes in 0..360, change to -180..180
        lon0 = lon0 - 360.0
    if lon0 > -111.0 and lon0 < -25.0:
        # goes East == goes 16
        return "goes16"
    elif lon0 > -170.0 and lon0 <= -111.0:
        # goes West == goes 17
        return "goes17"
    elif (lon0 >= 90.0 and lon0 <= 180) or (lon0 >= -180.0 and lon0 <= -170):
        # himawari8
        return "himawari8"
    else:
        return None


def region_from_stormID(stormID):
    si = stormID.upper()
    if si.startswith("A"):
        return "ATL"
    elif si.startswith("W"):
        return "WPAC"
    elif si.startswith("E"):
        return "EPAC"
    elif si.startswith("C"):
        return "CPAC"
    elif si.startswith("S"):
        return "SHEM"
    elif si.startswith("I"):
        return "IO"
    else:
        print(f"region not found for storm {stormID}, stopping")
        sys.exit()


def read_time_lat_lon_from_colorado_state(region=None, title=None, wwwPage=None):
    """
    year: of the cyclone
    region: of the cyclone
    title: male/female(other name of the cyclone
    wwwPage; from which data is scraped
    """
    response = requests.get(wwwPage)
    if response.status_code != 200:
        print(f"Sorry, bad response from {wwwPage}.")
        print(f"Response status code was: {response.status_code}.")
    dfs = pd.read_html(
        wwwPage
    )  # Returns list of all tables on page as pandas dataframes
    for df in dfs:
        print(df.iloc[0][0])
        if "Synoptic" in df.iloc[0][0]:
            df.columns = [
                df.iloc[0][0],
                df.iloc[0][1],
                df.iloc[0][2],
                df.iloc[0][3],
            ]
            df = df.iloc[1:]
            df.attrs = {"region": region, "title": title, "wwwPage": wwwPage}
            return df

    return None


def get_goes_aws_fileNames(
    currentDate, product="ABI-L1b-RadF", bucket=None, channel=None
):
    """
    get aws s3 file name (for later data download)
    to be sure, load some previous and next day files names, not to miss anything

    https://docs.opendata.aws/noaa-goes16/cics-readme.html

    @param currentDate: desired date-time for data to be downloaded
    @type currentDate: datetime.datetime object
    @param product: noaa product, currently only 'ABI-L1b-RadF' or 'ABI-L2-SSTF'
    @type product:
    @param bucket: either 'noaa-goes17' (GOES WEST) or 'noaa-goes16' (GOES EAST)
    @type bucket: string
    @param tag:  channel name to search for
    @type tag: string
    @return: name of the filePath (to be downloaded later)
    @rtype: string
    """
    # Use the anonymous credentials to access public data
    fs = s3fs.S3FileSystem(anon=True)
    product = "/" + product + "/"
    product = product.replace("//", "/")
    # get day number of the year
    dayNumber = currentDate.timetuple().tm_yday
    # get datetime for the next hour
    nextYear, nextDayNumber, nextHour = shifted_date_to_daynumber(
        mydatetime=currentDate, shiftHours=1.0
    )
    # get datetime for the previous hour
    (
        previousYear,
        previousDayNumber,
        previousHour,
    ) = shifted_date_to_daynumber(mydatetime=currentDate, shiftHours=-1.0)
    # print(f"download, product: {product}")
    # get all possible files that could be close in time, next and previous are because be may change day
    geo = (
        bucket
        + product
        + str(currentDate.year)
        + "/"
        + str(dayNumber).zfill(3)
        + "/"
        + str(currentDate.hour).zfill(2)
        + "/"
    )
    # print(f"geo {geo}")
    files = fs.ls(geo)
    # print("FILES0:", files)
    geoNext = (
        bucket
        + product
        + str(nextYear)
        + "/"
        + str(nextDayNumber).zfill(3)
        + "/"
        + str(nextHour).zfill(2)
        + "/"
    )
    # print(f"geoNext {geoNext}")
    # print("FILES0:", fs.ls(geoNext))
    files += fs.ls(geoNext)
    geoPrevious = (
        bucket
        + product
        + str(previousYear)
        + "/"
        + str(previousDayNumber).zfill(3)
        + "/"
        + str(previousHour).zfill(2)
        + "/"
    )
    files += fs.ls(geoPrevious)
    # print(f"geoPrev {geoPrevious}")
    # print("FILES0:", fs.ls(geoPrevious))
    # print(f"files {files}")
    if channel is not None:
        # filter only filenames containing the desired channel
        files = [f for f in files if channel in f]

    # get the file closest in time
    downloadFile = get_closest_goes_file_in_time(
        files=files, desiredDateTime=currentDate
    )
    # print("best:", downloadFile)
    downloadFile = "/".join(downloadFile.split("/")[1:])  # remove bucket name
    return [downloadFile]


def get_closest_goes_file_in_time(files=None, desiredDateTime=None):
    """get the closest file in time to the desiredDateTime
    filenames in GOES are in the form
    OR_ABI-L1b-RadM1-M3C01_G16_s20172511100550_e20172511101007_c20172511101048.nc
    sYYYYJJJHHMMSSm: year, day of year, hour, minute, second, tenth second
    """
    closest = None
    closestFile = None
    for file in files:
        # print(file.split('_')[-3])
        date_tmp = file.split("_")[-3][:-1]  # omit tenths of a second
        parsed = datetime.strptime(date_tmp, "s%Y%j%H%M%S")
        if closest is None:
            closest = parsed
            closestDT = abs((parsed - desiredDateTime).total_seconds())
            closestFile = file
            # print("0: closestDT:", closestDT)
        elif abs((parsed - desiredDateTime).total_seconds()) < closestDT:
            closest = parsed
            closestDT = abs((parsed - desiredDateTime).total_seconds())
            closestFile = file
            # print("closestDT:", closestDT)
    return closestFile


def get_himawari_aws_fileNames(
    currentDate,
    numberOfFiles,
    product,
    bucket,
    channel,
    accuracy=10,
):
    """
    get aws s3 file name (for later data download)
    to be sure, load some previous and next day files names, not to miss anything

    directory structure example:
    noaa-himawari8/AHI-L1b-FLDK/2020/06/30/0840/HS_H08_20200630_0840_B01_FLDK_R10_S0110.DAT.bz2

    @param currentDate: desired date-time for data to be downloaded
    @type currentDate: datetime.datetime object
    @param product: noaa product, currently only 'AHI-L1b-FLDK'
    @type product:
    @param bucket: currently only 'noaa-himawari8'
    @type bucket: string
    @param channel:  channel name to search for  B: B01 460 nm, G: B02 510 nm, R: B03 640 nm, IR: B13 10.4
    @type channel: string
    @return: name of the filePath (to be downloaded later)
    @rtype: string
    """
    # print(f"currentDate: {currentDate}")
    # print(f"HIMAWARI, {channel}")
    # Use the anonymous credentials to access public data
    fs = s3fs.S3FileSystem(anon=True)
    product = "/" + product + "/"
    product = product.replace("//", "/")
    # get all possible files that could be close in time, next and previous are because be may change day
    files = []
    icount = 0
    # round to previous 10 mins and add 10 mins
    tmpDate = round_to_x_mins(currentDate, accuracy=accuracy) + timedelta(
        minutes=accuracy
    )
    dt = -accuracy
    while (
        len(files) < numberOfFiles and icount <= 6
    ):  # we should get some data to proceed... take minimally 2 times
        geo = (
            bucket
            + product
            + str(tmpDate.year)
            + "/"
            + str(tmpDate.month).zfill(2)
            + "/"
            + str(tmpDate.day).zfill(2)
            + "/"
            + str(tmpDate.hour).zfill(2)
            + str(tmpDate.minute).zfill(2)
        )
        # print(f"geo {geo}")
        files = fs.ls(geo)
        if channel is not None:
            # filter only filenames containing the desired channel
            files = [f for f in files if channel in f]
        # files += files_tmp
        icount += 1
        tmpDate = tmpDate + timedelta(minutes=dt)
        dt = -2.0 * dt
        # print(f"Himawari files, icount: {len(files), icount, tmpDate}")

    # print(f"Himawari files: {len(files)}")

    # get the file closest in time
    bestFilePath = get_closest_himawari_file_in_time(
        files=files, desiredDateTime=currentDate
    )
    bestdir = "/".join(bestFilePath.split("/")[:-1])
    # print(f"all_files_in_best_dir: {bestdir}")
    all_files_in_dir = fs.ls(bestdir)
    # print(f"all_files_in_best_dir: {all_files_in_dir}")
    bestFile = bestFilePath.split("/")[-1]
    tag = "_".join(bestFile.split("_")[:-1])
    # print(f"tag {tag}")
    # because himawari data is split into 10 segments we need to grab the other files as well
    downloadFiles = [f for f in all_files_in_dir if tag in f]
    downloadFiles = [
        "/".join(f.split("/")[1:]) for f in downloadFiles
    ]  # remove bucket name

    # print("best files:", downloadFiles)

    return downloadFiles


def get_satellite(lon0, debug=False):
    """ lon may chang so that we must cange satellite... """
    # lon0 = pd.to_numeric(df["Longitude"]).mean()
    if (lon0 >= 85.0 and lon0 <= 180) or (lon0 >= -180.0 and lon0 <= -170):
        satellite = "himawari"
    elif lon0 < -25.0 and lon0 > -170.0:
        if lon0 < -111:
            # goes west
            satellite = "goes17"
        else:
            # goes east
            satellite = "goes16"
    else:
        print(
            f"we are outside of area of himawari8, goes16, goes17 lon: {lon0} . Stopping..."
        )
        return None
    return satellite


def get_satellite_data(satellite, channelInit):
    """

    Parameters
    ----------
    satellite : which satellite
    channelInit : which channel

    Returns
    -------

    """
    if satellite == "himawari":
        product = "AHI-L1b-FLDK"
        bucket = "noaa-himawari8"
        numberOfFiles = 10
        reader = "ahi_hsd"
        reader_kwargs = {"mask_space": False}
        if channelInit == "ir":
            channel = "B13"
        elif channelInit == "rainfall":
            product = "AHI-L2-FLDK-RainfallRate"
            numberOfFiles = 1
            channel = "RAIN_RATE"
            reader = "abi_l2_nc"
        else:
            print(f"WARNING: not implemented {channelInit}, returning the init channel")
            channel = channelInit
    elif satellite in ["goes16", "goes17"]:
        product = "ABI-L1b-RadF"
        # product = "ABI-L2-MCMIPC"
        bucket = "noaa-goes16"
        numberOfFiles = 1
        reader = "abi_l1b"
        reader_kwargs = {}
        if channelInit == "ir":
            channel = "C13"
        elif channelInit == "rainfall":
            product = "ABI-L2-RRQPEF"
            numberOfFiles = 1
            channel = 'RRQPE'  # satpy
            #channel = "RRQPEF"
            reader = "abi_l2_nc"
        else:
            print(f"WARNING: not implemented {channelInit}, returning the init channel")
            channel = channelInit

    return bucket, product, channel, reader, reader_kwargs, numberOfFiles


def get_closest_himawari_file_in_time(files=None, desiredDateTime=None):
    """
    get the closest file in time to the desiredDateTime
    filenames in Himawari8 are in the form
    HS_H08_20200702_0620_B01_FLDK_R10_S0110.DAT.bz2
    XX_XXX_YYYYMMDD_HHMM_...: year, month, day, hour, minute
    """
    print(f"files {files}")
    closest = None
    closestFile = None
    for file in files:
        # print(file.split('_')[-3])
        # date_tmp = file.split("_")[2] + file.split("_")[3]
        # parsed = datetime.strptime(date_tmp, "%Y%m%d%H%M")
        date_tmp = file.split("/")
        parsed = datetime(
            year=int(date_tmp[2]),
            month=int(date_tmp[3]),
            day=int(date_tmp[4]),
            hour=int(date_tmp[5][0:2]),
            minute=int(date_tmp[5][2:4]),
        )
        print(f"parsed{parsed}")
        if closest is None:
            closest = parsed
            closestDT = abs((parsed - desiredDateTime).total_seconds())
            closestFile = file
            # print("0: closestDT:", closestDT)
        elif abs((parsed - desiredDateTime).total_seconds()) < closestDT:
            closest = parsed
            closestDT = abs((parsed - desiredDateTime).total_seconds())
            closestFile = file
            # print("closestDT:", closestDT)
    return closestFile


def upload_aws(localFilePath, bucketName, awsDir):
    """upload from local disk to AWS S3, file path should be like:
    ieee-temp/extra-images/2017/ATL/11L/visir/20170908.1215.GOES13.vis.11L.IRMA.ATL.png
    upload_aws('/localDir' + filename, '<bucket-name>', 'awsFoldersAndFilename')

    """
    filePath = Path(localFilePath)
    fileName = Path(localFilePath).name
    awsPath = str(Path(awsDir).joinpath(fileName))
    bucketName = str(bucketName)
    # logging.info(f"Uploading {fileName} to {awsPath} (bucket {bucketName})")

    s3_client = boto3.client("s3")
    try:
        response = s3_client.upload_file(str(filePath), bucketName, awsPath)
    except ClientError as e:
        print(f"Problem with upload to aws, error: {e}")
        return False
