"""
from cyclone's lat lon as a function of time,
download the corresponding images
and put them into a aws
#missing ones
python satpyCycloneToAws.py --stormId al092020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId al202020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId al202020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId ep092020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId wp012020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId wp032020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId wp092020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId wp102020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

python satpyCycloneToAws.py --stormId wp112020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 &

# short slice of al29
python satpyCycloneToAws.py --stormId al272020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 --datetimeInit "2020-10-25 20:00:00" --datetimeFinal "2020-10-25 21:00:00" \
     --showLatLonGrid True

# short slice of haishen
python satpyCycloneToAws.py --stormId wp112020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 --datetimeInit "2020-09-07 00:00:00" --datetimeFinal "2020-09-07 00:30:00"

# short slice of Bavi
python satpyCycloneToAws.py --stormId wp092020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 --datetimeInit "2020-08-27 11:30:00" --datetimeFinal "2020-08-27 12:30:00"

# short slice of Delta
python satpyCycloneToAws.py --stormId al262020 --dMinutes 30 --xsize 1024 --ysize 1024 \
    --channel ir --resolution 2.0 --datetimeInit "2020-10-06 19:00:00" --datetimeFinal "2020-10-06 20:00:00" \
    --showLatLonGrid True

"""
from predict import Predict
import datetime
import pandas as pd
import boto3
from pathlib import Path


import os
import sys
from time import sleep
from cyclonesatpy.iohelp import (
    region_from_stormID,
    read_time_lat_lon_from_colorado_state,
    get_himawari_aws_fileNames,
    get_goes_aws_fileNames,
    get_satellite,
    get_satellite_data,
    upload_aws,
)
from cyclonesatpy.timehelp import parse_colorado_state_time
from cyclonesatpy.mischelp import (
    windspeed_to_category,
    clean_local_dir_except_files_ending,
)
from cyclonesatpy.maphelp import make_and_save_satpy_image_colorize_manual


class SatpyCycloneToAws:
    def __init__(
        self,
        homeDir,
        inDir,
        outDir,
        stormId,
        dMinutes,
        datetimeInit=None,
        datetimeFinal=None,
    ):
        """

        Parameters
        ----------
        homeDir :   other dirs are with respect to this directory (if not None)
        inDir :     dir to download data
        outDir :    dir to write data
        csvFile :   csv file that results from the process
        stormId :   official storm id, like al072020
        dMinutes :  how many minutes between images
        datetimeInit : initial datetime to start record
        datetimeFinal : final dateime to record
        """
        if homeDir is None or homeDir.upper() == "HOME":
            self.homeDir = Path.home()
            self.outDir = os.path.join(self.homeDir, outDir)
            self.inDir = os.path.join(self.homeDir, inDir)
        else:
            self.homeDir = homeDir
            self.outDir = outDir
            self.inDir = inDir

        self.csvFile = stormId + ".csv"
        self.stormId = stormId
        self.s3 = boto3.resource("s3")

        (
            self.datetimeInit,
            self.datetimeFinal,
            self.dMinutes,
            self.pr,
            self.region,
        ) = self.get_initial_data(
            stormId=stormId,
            datetimeInit=datetimeInit,
            datetimeFinal=datetimeFinal,
            dMinutes=dMinutes,
        )

    def cut_times_init(self, df, datetimeInit):
        mask = df["Synoptic Time"] > datetimeInit
        return df.loc[mask]

    def get_initial_data(
        self,
        stormId,
        datetimeInit,
        datetimeFinal,
        dMinutes,
        wwwPageInit="https://rammb-data.cira.colostate.edu/tc_realtime/storm.asp?storm_identifier=",
    ):
        region = region_from_stormID(stormID=stormId)
        wwwPage = wwwPageInit + stormId
        print("READING:", region, stormId, wwwPage)
        df = read_time_lat_lon_from_colorado_state(
            region=region, title=stormId, wwwPage=wwwPage
        )
        pr = Predict(df=df)
        pr.init_colorado_state()
        df["Synoptic Time"] = pd.to_datetime(df["Synoptic Time"])
        if datetimeInit is None:
            datetimeInit = parse_colorado_state_time(pr.times[0])
        if datetimeFinal is None:
            datetimeFinal = parse_colorado_state_time(pr.times[-1])
        print(datetimeInit)
        df = df.loc[df["Synoptic Time"] > datetimeInit]
        df = df.loc[df["Synoptic Time"] < datetimeFinal]
        print(df)

        return datetimeInit, datetimeFinal, dMinutes, pr, region

    def cyclone_to_aws(
        self,
        channelInit,
        xsize,
        ysize,
        resolution,
        proj,
        outAwsBucket,
        outAwsDir,
        showLatLonGrid,
        keepFiles=[".png", ".npy", ".json"],
        maxTry=5,
        uploadAws=True,
        debug=True,
    ):
        """
        Download files from aws,
        use satpyt to generate image based on downloaded file
        upload image to aws
        Parameters
        ----------
        showLatLonGrid : T/F should we show lat lon grid?
        uploadAws : should we upload results to aws
        keepFiles : which local file endings are NOT deleted
        maxTry :    how many times we try to download
        channelInit : which satellite channel we use to mke image
        """
        dictionaries = []
        datetimeCurrent = self.datetimeInit
        while datetimeCurrent < self.datetimeFinal:
            lat, lon = self.pr.get_interpolated_lat_lon(dateTime=datetimeCurrent)
            print(f"time now:{datetimeCurrent} lat, lon now:{lat, lon}")

            satellite = get_satellite(lon0=lon)
            intensity = int(round(self.pr.get_intensity(dateTime=datetimeCurrent)))
            # download files from aws to local disk
            ok = False
            failed = False
            tryOut = 0
            bucket, product, channel, reader, numberOfFiles = (
                None,
                None,
                None,
                None,
                None,
            )
            localFiles = []

            while not ok:
                try:
                    (
                        bucket,
                        product,
                        channel,
                        reader,
                        reader_kwargs,
                        numberOfFiles,
                    ) = get_satellite_data(satellite=satellite, channelInit=channelInit)
                    print("calling")
                    if satellite == "himawari":
                        awsfilenames = get_himawari_aws_fileNames(
                            currentDate=datetimeCurrent,
                            numberOfFiles=numberOfFiles,
                            product=product,
                            bucket=bucket,
                            channel=channel,
                        )
                    elif satellite in ["goes16", "goes17"]:
                        awsfilenames = get_goes_aws_fileNames(
                            currentDate=datetimeCurrent,
                            product=product,
                            bucket=bucket,
                            channel=channel,
                        )
                    else:
                        print(
                            f"only satelites 'himawari', 'goes16', 'goes17' implemented"
                        )
                    print("did call ")

                    myBucket = self.s3.Bucket(bucket)
                    localFiles = []
                    for awsfilename in awsfilenames:
                        localFile = os.path.join(self.inDir, awsfilename.split("/")[-1])
                        print("localfile,", localFile)
                        print("aws file: ", awsfilename)
                        if Path(os.path.join(localFile)).is_file():
                            print(f"file {localFile} exists, not downloading")
                            localFiles.append(localFile)
                            continue
                        else:
                            print(f"Downloading  {localFile}")
                        myBucket.download_file(
                            awsfilename,
                            localFile,
                        )
                        localFiles.append(localFile)
                    ok = True
                except Exception as e:
                    ok = False
                    tryOut += 1
                    print(f"error: {e}")
                    print("problem with download, trying again...")
                    sleep(5)
                    clean_local_dir_except_files_ending(
                        localDir=self.outDir, keepEndings=keepFiles
                    )
                    if tryOut > maxTry:
                        print(
                            f"download failed, ntry reached, giving up, bye... storm {self.stormId}"
                        )
                        failed = True
                        ok = True

            if not failed:
                print(f"{self.stormId}")
                outFileName = (
                    str(self.stormId)
                    + "_"
                    + str(datetimeCurrent)
                    + "_"
                    + str(intensity)
                    + "kt_"
                    + satellite
                    + "_"
                    + "lat"
                    + str(round(float(lat), 4))
                    + "_"
                    + "lon"
                    + str(round(float(lon), 4))
                    + ".jpg"
                )
                outFileName = outFileName.replace(" ", "-").replace(":", "-")
                print(f"out:{outFileName}")
                # build the image using satpy
                (
                    ourfilename,
                    imgOur,
                    npimg,
                    data,
                ) = make_and_save_satpy_image_colorize_manual(
                    localFiles=localFiles,
                    lat_0=lat,
                    lon_0=lon,
                    xsize=xsize,
                    ysize=ysize,
                    resolution=resolution,
                    proj=proj,
                    reader=reader,
                    reader_kwargs=reader_kwargs,
                    channel=channel,
                    outDir=self.outDir,
                    outFileName=outFileName,
                    shift=-273.15,
                    showCoast=True,
                    coastColor=(241, 171, 0),
                    showLatLonGrid=False,
                    gridColor="white",
                )
                if uploadAws:
                    upload_aws(
                        localFilePath=os.path.join(self.outDir, outFileName),
                        bucketName=outAwsBucket,
                        awsDir=os.path.join(outAwsDir, self.stormId),
                    )
                if not debug:
                    clean_local_dir_except_files_ending(
                        localDir=self.outDir, keepEndings=keepFiles
                    )

                dictionaries.append(
                    {
                        "img": outFileName,
                        "dir": os.path.join(outAwsBucket, outAwsDir, self.stormId),
                        "storm_name": self.stormId,
                        "date": datetimeCurrent,
                        "windspeed": intensity,
                        "category": windspeed_to_category(intensity),
                        "region": self.region,
                        "year": datetimeCurrent.year,
                        "satellite": satellite,
                        "lat": round(float(lat), 4),
                        "lon": round(float(lon), 4),
                    }
                )
                df = pd.DataFrame(dictionaries)
                df.to_csv(os.path.join(self.outDir, self.csvFile))
                if uploadAws:
                    upload_aws(
                        localFilePath=os.path.join(self.outDir, self.csvFile),
                        bucketName=outAwsBucket,
                        awsDir=os.path.join(outAwsDir, self.stormId),
                    )
            datetimeCurrent = datetimeCurrent + datetime.timedelta(
                minutes=self.dMinutes
            )


if __name__ == "__main__":
    from cyclonesatpy.mischelp import str2bool
    import argparse
    from pathlib import Path
    from dateutil import parser as dparser

    parser = argparse.ArgumentParser()

    parser.add_argument("--stormId", help="Storm name, use official id")
    parser.add_argument(
        "--homeDir", help="upmost local reference directory", default="HOME"
    )
    parser.add_argument(
        "--inDir",
        help="local dir where to download from aws (with respect to self.homeDir)",
        default="Downloads",
    )
    parser.add_argument(
        "--outDir",
        help="local dir where to write data (with respect to self.homeDir)",
        default="Downloads",
    )

    parser.add_argument(
        "--dMinutes", help="time step between satellite images, in minutes", type=int
    )
    parser.add_argument("--datetimeInit", help="init time to start recording", type=str)
    parser.add_argument(
        "--datetimeFinal", help="final time to stop recording", type=str
    )
    parser.add_argument(
        "--uploadAws",
        type=str2bool,
        nargs="?",
        const=True,
        default=True,
        help="Should we upload to to aws?",
    )

    parser.add_argument(
        "--showLatLonGrid",
        type=str2bool,
        nargs="?",
        const=False,
        default=False,
        help="Show latlon lines in the image",
    )

    parser.add_argument(
        "--outAwsBucket",
        help="aws upload uppermost directory",
        # default="hu-vals/val2020",
        default="hu-vals",
    )

    parser.add_argument(
        "--outAwsDir",
        help="aws upload uppermost directory",
        default="val2020",
    )

    parser.add_argument(
        "--channel", help="Which channel to make image ir/vis/singleVis"
    )
    parser.add_argument(
        "--xsize", help="output image x size in pixels", type=int, default=1024
    )
    parser.add_argument(
        "--ysize", help="output image y size in pixels", type=int, default=1024
    )
    parser.add_argument(
        "--resolution",
        help="resolution of the output image in km",
        type=float,
    )
    parser.add_argument(
        "--proj", help="output map projection", type=str, default="merc"
    )

    args = parser.parse_args()
    if args.datetimeInit is None:
        datetimeInit = None
    else:
        datetimeInit = dparser.parse(args.datetimeInit)
        print(f"datetimeinit: {datetimeInit}")
    if args.datetimeFinal is None:
        datetimeFinal = None
    else:
        datetimeFinal = dparser.parse(args.datetimeFinal)
    sca = SatpyCycloneToAws(
        homeDir=args.homeDir,
        inDir=args.inDir,
        outDir=args.inDir,
        stormId=args.stormId,
        dMinutes=args.dMinutes,
        datetimeInit=datetimeInit,
        datetimeFinal=datetimeFinal,
    )
    assert args.channel in [
        "ir",
        "visRGB",
        "vis1ch",
    ], f"channel {args.channel} not implemented, use one of: 'ir', 'visRGB', 'vis1ch'"

    sca.cyclone_to_aws(
        uploadAws=args.uploadAws,
        channelInit=args.channel,
        xsize=args.xsize,
        ysize=args.ysize,
        resolution=args.resolution,
        proj=args.proj,
        outAwsBucket=args.outAwsBucket,
        outAwsDir=args.outAwsDir,
        showLatLonGrid=args.showLatLonGrid,
    )
