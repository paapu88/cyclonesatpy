"""
routines that have to do with drawing of the map ...
"""

import os
import sys
import numpy as np
from pathlib import Path
from pyproj import Proj, pj_list
import geopy
import geopy.distance

import satpy
from pycoast import ContourWriterAGG
from PIL import Image
from trollimage.image import Image as TrollImage
from trollimage.colormap import palettize as mypalettize
from trollimage.colormap import rdbu, spectral
import aggdraw
import pprint

from trollimage.colormap import greys
from pyresample.area_config import get_area_def
import matplotlib as mpl
from cyclonesatpy.mischelp import plot_rain_rate_image

from cyclonesatpy.mischelp import replace_nans
import matplotlib.pyplot as plt


def colors_function(data, color_high, color_low, datamin, datamax):
    """ from data values(float) in scale 0..1 for this color range,  to color values(uint8) [color_high, color_low] """
    data = (data - datamin) / (datamax - datamin)
    data = color_high - data * (color_high - color_low)
    return np.around(data).astype(np.uint8)


def colorize_manual(
    data,
    mmin=-80.0,
    mmax=30.0,
):
    data = np.array(data)
    maskmin = data <= mmin
    maskmax = data >= mmax
    maskother = (data > mmin) & (data < mmax)
    # data to 0..1 float
    data[maskother] = (data[maskother] - mmin) / (mmax - mmin)
    if maskother.any():
        data[maskother] = colors_function(
            data[maskother], color_high=255, color_low=60, datamin=0.0, datamax=1.0
        )

    data[maskmin] = 255
    data[maskmax] = 255 - 2 * (mmax - mmin)
    data = np.around(data).astype(np.uint8)
    return data

def colors_function_mapping_real(data, x0, color_high=255, color_low=0):
    """ from data values(float) in scale 0..1 for this color range,
    color value map is in x0
    to color values(uint8) [color_high, color_low] """
    bin = color_low + np.around(data*(color_high-color_low)).astype(np.uint8)
    print(bin)
    return np.around(x0[bin]*(color_high-color_low)).astype(np.uint8)

def colors_function_mapping(data):
    """ from data values(float) in scale 0..1 for this color range,
    color value map is in x0, here x0 is integer 0..255 """
    #bin = bin_low + np.around(data*(bin_high-bin_low)*255).astype(np.uint16)
    return 255-np.around(data*255)

def bd_enhancemet(data, min=30):
    mask1 = data <= min
    mask2 = ~mask1
    data[mask1] = 0
    data[mask2] = np.around((data[mask2]-min)/(255-min)*255).astype(np.uint16)
    return data

def colorize_mapping(
    data,
    x0,
    mmin=-80.0,
    mmax=30.0,
):
    data = np.array(data)
    maskmin = data <= mmin
    maskmax = data >= mmax
    maskother = (data > mmin) & (data < mmax)
    # data to 0..1 float
    data[maskother] = (data[maskother] - mmin) / (mmax - mmin)
    if maskother.any(): # 0..255
        data[maskother] = colors_function_mapping(
            data = data[maskother]
        )
        #print(f"data[maskother] {data[maskother]}")
        #data[maskother] = bd_enhancemet(data[maskother])

    data[maskmin] = 255
    data[maskmax] = 0
    data = np.around(data).astype(np.uint8)
    return data


def colorize_manual_4(
    data,
    mmin=-90.0,
    mmax=30.0,
    border1=28.0 / 110.0,
    border2=84.0 / 110.0,
    border3=99.0 / 110.0,
    border4=105.0 / 110.0,
):
    """
    Trying to reproduce navymil jpg images from satpy -80C .. +30C ir colors
    Parameters
    ----------
    data : satpy -80C .. +30C ir colors
    mmin : satpy data min (less than this will be white)
    mmax : satpy data max (more than this will be black)
    border1 : region limit in data (for color mapping), first peak
    border2 : region limit in data (for color mapping), though before peak
    border3 : region limit in data (for color mapping), peak
    border4 : region limit in data (for color mapping), though after peak

    Returns
    -------

    """
    data = np.array(data)
    maskmin = data <= mmin
    maskmax = data >= mmax
    maskother = (data > mmin) & (data < mmax)
    # data to 0..1 float
    data[maskother] = (data[maskother] - mmin) / (mmax - mmin)
    maskWhiteWhite = data <= border1
    maskWhite = (data > border1) & (data <= border2)
    maskPeakWhite = (data > border2) & (data <= border3)
    maskPeakBlack = (data > border3) & (data <= border4)
    maskBlack = data > border4
    if maskWhiteWhite.any():
        data[maskWhiteWhite] = colors_function(
            data[maskWhiteWhite],
            color_high=255,
            color_low=226,
            datamin=0.0,
            datamax=border1,
        )
    if maskWhite.any():
        data[maskWhite] = colors_function(
            data[maskWhite],
            color_high=226,
            color_low=125,
            datamin=border1,
            datamax=border2,
        )
    if maskPeakWhite.any():
        data[maskPeakWhite] = colors_function(
            data[maskPeakWhite],
            color_high=125,
            color_low=84,
            datamin=border2,
            datamax=border3,
        )
    if maskPeakBlack.any():
        data[maskPeakBlack] = colors_function(
            data[maskPeakBlack],
            color_high=84,
            color_low=60,
            datamin=border3,
            datamax=border4,
        )
    if maskBlack.any():
        data[maskBlack] = colors_function(
            data[maskBlack], color_high=60, color_low=0, datamin=border4, datamax=1.0
        )

    data[maskmin] = 255
    # data[maskmax] = 255 - 2 * (mmax - mmin)
    data[maskmax] = 0
    data = np.around(data).astype(np.uint8)
    return data


def make_and_save_satpy_image_colorize_manual(
    localFiles,
    lat_0,
    lon_0,
    xsize,
    ysize,
    resolution,
    proj,
    reader,
    reader_kwargs,
    channel,
    outDir,
    outFileName,
    shift,
    showCoast=True,
    coastColor=(255, 0, 0),
    showLatLonGrid=False,
    gridColor="white",
    gridFont="/usr/share/fonts/truetype/liberation/LiberationMono-Bold.ttf",
):
    """
    define the area
    Parameters
    ----------
    localFiles : from which local file we build image
    lat_0 : center latitude in output-image
    lon_0 : center longitude in output-image
    xsize : xsize of the output image in pixels
    ysize : ysize of the output image in pixels
    resolution : resolution of the output image in km
    proj : map projection of the output image
    reader: satellite reader for satpy
    reader_kwargs: satellite reader parameters for satpy
    channel: satellite channel
    outDir: into which dir to put the result
    outFileName: name of the map image file
    shift: shift data by this number

    shapefiles for coastlines are downloaded from
    https://www.ngdc.noaa.gov/mgg/shorelines/data/gshhg/latest/
    (*shp*zip)
    """
    # debug_on()

    resolution = float(resolution)
    # print(f"proj {proj}, resolution {resolution} ")
    p = Proj(proj=proj, lat_0=lat_0, lon_0=lon_0)
    # calculates only lon distances
    distX = 0.5 * xsize * resolution  # in km
    distY = 0.5 * ysize * resolution  # in km
    gp_distX = geopy.distance.distance(kilometers=distX)
    gp_distY = geopy.distance.distance(kilometers=distY)
    west = gp_distX.destination(
        point=geopy.Point((lat_0, lon_0)), bearing=270
    )  ## 270 = WEST
    east = gp_distX.destination(point=geopy.Point((lat_0, lon_0)), bearing=90)
    north = gp_distY.destination(point=geopy.Point((lat_0, lon_0)), bearing=0)
    south = gp_distY.destination(point=geopy.Point((lat_0, lon_0)), bearing=180)
    left, top = p(west[1], south[0])
    right, bottom = p(east[1], north[0])
    area_extent = (left, top, right, bottom)
    proj4_str = f"+proj={proj} +lat_0={lat_0} +lon_0={lon_0} +ellps=WGS84"
    # debug_on()
    areadef = get_area_def(
        area_id="_lat" + str(lat_0) + "_lon" + str(lon_0),
        area_name="dummy",
        proj_id=proj,
        proj4_args=proj4_str,
        width=xsize,
        height=ysize,
        area_extent=area_extent,
    )
    scn = satpy.Scene(
        localFiles, reader=reader, reader_kwargs=reader_kwargs, sensor="abi"
    )
    scn.load([channel])
    # print(f"avaiable: {scn.available_dataset_names()} reader: {reader}")
    local_scene = scn.resample(areadef)
    local_scene[channel] = local_scene[channel] + shift  # to celcius
    data = local_scene[channel].values
    data = replace_nans(data)
    print(f"data min max: {np.min(data), np.max(data)}")
    npimg = colorize_manual_4(
        data,
        mmin=-90.0,
        mmax=30.0,
    )
    img = Image.fromarray(npimg).convert("RGB")  # PIL image, to rgb
    if showCoast or showLatLonGrid:
        cw = ContourWriterAGG(
            os.path.join(Path.home(), "git/cyclonesatpy/cyclonesatpy/dev/shapefiles/")
        )
        if showCoast:
            cw.add_coastlines(
                img, areadef, resolution="i", level=4, outline=coastColor, width=1
            )
        if showLatLonGrid:
            font = aggdraw.Font(
                gridColor,
                gridFont,
                opacity=127,
                size=16,
            )
            cw.add_grid(
                img,
                areadef,
                (2.0, 2.0),
                (1.0, 1.0),
                font,
                fill=gridColor,
                outline=gridColor,
                minor_outline=gridColor,
            )
    img.convert("RGB").save(os.path.join(outDir, outFileName))
    return os.path.join(outDir, outFileName), img.convert("RGB"), npimg, data





def make_and_save_satpy_image_colorize_matplotlib(
    localFiles,
    lat_0,
    lon_0,
    xsize,
    ysize,
    resolution,
    proj,
    reader,
    reader_kwargs,
    channel,
    outDir,
    outFileName,
    shift=0.0,
    colormax=100.0, # data is divided with this
    colormin=0.0,
    colorMap='gist_rainbow',
    showCoast=True,
    coastColor=(255, 0, 0),
    showLatLonGrid=False,
    gridColor="white",
    gridFont=os.path.join(Path.home(), "git/cyclonesatpy/cyclonesatpy/dev/LiberationMono-Bold.ttf"),
):
    """
    define the area
    Parameters
    ----------
    localFiles : from which local file we build image
    lat_0 : center latitude in output-image
    lon_0 : center longitude in output-image
    xsize : xsize of the output image in pixels
    ysize : ysize of the output image in pixels
    resolution : resolution of the output image in km
    proj : map projection of the output image
    reader: satellite reader for satpy
    reader_kwargs: satellite reader parameters for satpy
    channel: satellite channel
    outDir: into which dir to put the result
    outFileName: name of the map image file
    shift: shift data by this number
    colormax: this datavalue is scaled to 1.0 for color images
    colormin: this datavalue is scaled to 0.0 for color images

    shapefiles for coastlines are downloaded from
    https://www.ngdc.noaa.gov/mgg/shorelines/data/gshhg/latest/
    (*shp*zip)
    """
    # debug_on()

    resolution = float(resolution)
    # print(f"proj {proj}, resolution {resolution} ")
    p = Proj(proj=proj, lat_0=lat_0, lon_0=lon_0)
    # calculates only lon distances
    distX = 0.5 * xsize * resolution  # in km
    distY = 0.5 * ysize * resolution  # in km
    gp_distX = geopy.distance.distance(kilometers=distX)
    gp_distY = geopy.distance.distance(kilometers=distY)
    west = gp_distX.destination(
        point=geopy.Point((lat_0, lon_0)), bearing=270
    )  ## 270 = WEST
    east = gp_distX.destination(point=geopy.Point((lat_0, lon_0)), bearing=90)
    north = gp_distY.destination(point=geopy.Point((lat_0, lon_0)), bearing=0)
    south = gp_distY.destination(point=geopy.Point((lat_0, lon_0)), bearing=180)
    left, top = p(west[1], south[0])
    right, bottom = p(east[1], north[0])
    area_extent = (left, top, right, bottom)
    proj4_str = f"+proj={proj} +lat_0={lat_0} +lon_0={lon_0} +ellps=WGS84"
    # debug_on()
    areadef = get_area_def(
        area_id="_lat" + str(lat_0) + "_lon" + str(lon_0),
        area_name="dummy",
        proj_id=proj,
        proj4_args=proj4_str,
        width=xsize,
        height=ysize,
        area_extent=area_extent,
    )
    scn = satpy.Scene(
        localFiles, reader=reader, reader_kwargs=reader_kwargs, sensor="abi"
    )
    scn.load([channel])
    print(scn)
    print(f"avaiable: {scn.available_dataset_names()} reader: {reader}")
    local_scene = scn.resample(areadef)
    local_scene[channel] = local_scene[channel] + shift
    data = local_scene[channel].values
    data = replace_nans(data)
    print(f"data minnn: {np.nanmin(data)}")
    print(f"data maxxx: {np.nanmax(data)}")
    """
    datatest = data.copy()
    mask = datatest>50
    datatest[mask]=0
    datatest[~mask]=255
    imgtest=Image.fromarray(datatest)
    imgtest.show()
    """
    datascaled = (data - colormin)/ (colormax-colormin)
    datascaled = np.clip(datascaled, a_min=0.0, a_max=1.0)
    # print(f"data min max: {np.min(data), np.max(data)}")
    #npimg = colorize_mapping(
    #    data,
    #    x0,
    #    mmin=-90.0,
    #    mmax=30.0,
    #)
    # Get the color map by name:
    if colorMap is None:
        print("own colormap!")
        boundaries, cm, norm = plot_rain_rate_image(data, name=None, plot=False)
    else:
        cm = plt.get_cmap(colorMap)
    # Apply the colormap like a function to any array:
    plt.imshow(data, cmap=cm, norm=norm)
    plt.show()
    npimg = cm(datascaled)
    print(f"data npimg: {np.nanmin(npimg)}")
    print(f"data npimg: {np.nanmax(npimg)}")
    print(npimg.shape)
    img = Image.fromarray((255.0*npimg[:,:,0:3]).astype(np.uint8))
    img.show()
    print(f"gridfont: {gridFont}")
    #img = Image.fromarray(npimg).convert("RGB")  # PIL image, to rgb
    if showCoast or showLatLonGrid:
        cw = ContourWriterAGG(
            os.path.join(Path.home(), "git/cyclonesatpy/cyclonesatpy/dev/shapefiles/")
        )
        if showCoast:
            cw.add_coastlines(
                img, areadef, resolution="i", level=4, outline=coastColor, width=1
            )
        if showLatLonGrid:
            font = aggdraw.Font(
                gridColor,
                gridFont,
                opacity=127,
                size=16,
            )
            cw.add_grid(
                img,
                areadef,
                (2.0, 2.0),
                (1.0, 1.0),
                font,
                fill=gridColor,
                outline=gridColor,
                minor_outline=gridColor,
            )
    img.convert("RGB").save(os.path.join(outDir, outFileName))
    #img.save(os.path.join(outDir, outFileName))
    return os.path.join(outDir, outFileName), img.convert("RGB"), npimg, data


def list_projection(tag=None):
    pp = pprint.PrettyPrinter(indent=4)
    if tag is None:
        pp.pprint(pj_list)


def kill_day_border(lons):
    """ make lons continious for interpolation """
    if min(lons) < -150.0 and max(lons) > 150.0:
        lons = [l if l > 0 else l + 360.0 for l in lons]
    return lons


def awake_day_border(lons):
    """ lons to -180..0..180"""
    if max(lons) > 180.0:
        lons = [l if l <= 180 else l - 360.0 for l in lons]
    return lons


def latlon2samelength(ll):
    """ navymil string to lat/lon """
    if len(ll) == 2:
        return "000" + ll
    if len(ll) == 3:
        return "00" + ll
    elif len(ll) == 4:
        return "0" + ll
    elif len(ll) == 5:
        return ll
    # print(f"wrong lat long string: {ll}")
    return None


def get_navymil_lat(filename):
    """ navymil string to lat """
    lat = filename.split("-")[-2]
    lat = latlon2samelength(lat)
    if lat is None:
        return np.nan
    if lat.endswith("N"):
        return float(lat[0:4]) / 10.0
    elif lat.endswith("S"):
        return -float(lat[0:4]) / 10.0
    else:
        return np.nan


def get_navymil_lon(filename):
    """ navymil string to lon """
    lon = filename.split("-")[-1].split(".")[0]
    lon = latlon2samelength(lon)
    if lon is None:
        return np.nan
    if lon.endswith("E"):
        return float(lon[0:4]) / 10.0
    elif lon.endswith("W"):
        return -float(lon[0:4]) / 10.0
    else:
        return np.nan


def get_navymil_lat_lon(filename):
    """ navymil string to lat/lon """
    lat = filename.split("-")[-2]
    lat = latlon2samelength(lat)
    if lat is None:
        return np.nan
    lon = filename.split("-")[-1].split(".")[0]
    lon = latlon2samelength(lon)
    if lon is None:
        return np.nan
    if lat.endswith("N"):
        lat = float(lat[0:4]) / 10.0
    elif lat.endswith("S"):
        lat = -float(lat[0:4]) / 10.0
    else:
        print("error in navymil lat")
        sys.exit()
    if lon.endswith("E"):
        lon = float(lon[0:4]) / 10.0
    elif lon.endswith("W"):
        lon = -float(lon[0:4]) / 10.0
    else:
        print("error in navymil lon")
        sys.exit()
    return lat, lon

