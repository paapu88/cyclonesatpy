"""
routines that did not fit to any category...
"""

import argparse
import requests
import pandas as pd
import datetime
import s3fs
import glob
import bz2
import boto3
from datetime import datetime, timedelta
import os
import numpy as np
from pathlib import Path
from pyproj import Proj, pj_list
import geopy
import geopy.distance

import satpy
from satpy.writers import to_image
from satpy.enhancements import colorize
from satpy.utils import debug_on
from pycoast import ContourWriterAGG
from PIL import Image, ImageFont
from trollimage.image import Image as TrollImage
from trollimage.colormap import palettize as mypalettize
from trollimage.colormap import greys

import aggdraw
import pprint
import yaml
import io

from trollimage.colormap import spectral, greys
from pyresample.area_config import get_area_def
from botocore.exceptions import ClientError

# color definitions to match navymil, data range -80C ... +30C
# colors = np.linspace(255, 255 - 2 * (30 + 80), 2 * (30 + 80))


def parse_colorado_state_time(dateTimeStr=None):
    return datetime.strptime(dateTimeStr, "%Y-%m-%d %H:%M")


def get_navymil_time(filename):
    """ get datetime from navymil filename """
    mydate = filename.split(".")[0]
    mytime = filename.split(".")[1]
    return datetime(
        year=int(mydate[0:4]),
        month=int(mydate[4:6]),
        day=int(mydate[6:8]),
        hour=int(mytime[0:2]),
        minute=int(mytime[2:4]),
    )


def round_to_x_mins(tm, accuracy=10):
    """
    https://stackoverflow.com/questions/3463930/how-to-round-the-minute-of-a-datetime-object
    Parameters
    ----------
    tm : datetime
    x:   rounding accuracy in minutes
    Returns
    -------
    datetime rounded to x min
    """
    discard = timedelta(
        minutes=tm.minute % accuracy, seconds=tm.second, microseconds=tm.microsecond
    )
    tm -= discard
    if discard >= timedelta(minutes=accuracy // 2):
        tm += timedelta(minutes=accuracy)
    return tm


def shifted_date_to_daynumber(mydatetime=None, shiftHours=1.0):
    """
    shift date time in time and
    get year, number of day in a year, hour (all for the shifted datetime)

    @param mydatetime:  a date and time
    @type mydatetime: datetime.datetime object (naive)
    @param shiftHours: how many minutes to shift the time to future (>0.0) or to past (<0.0)
    @type shiftHours: float
    @return: year, number of day in a year, hour (all for the shifted datetime)
    @rtype: float, float, float ? or int, int, int
    """

    mydatetime = mydatetime + timedelta(hours=shiftHours)
    return (
        mydatetime.year,
        mydatetime.timetuple().tm_yday,
        mydatetime.hour,
    )
