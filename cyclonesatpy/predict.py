"""
predict values based on interpolation

"""

from timehelp import parse_colorado_state_time
from maphelp import kill_day_border, awake_day_border
import numpy as np
import scipy.interpolate


class Predict:
    def __init__(self, df=None):
        self.df = df

    def init_colorado_state(self):
        """
        colorado state
        pandas dateframe to numpy arrays, also set time going forwards
        """
        self.times = np.squeeze(self.df[["Synoptic Time"]].to_numpy())[::-1]  # string
        self.lats = np.squeeze(self.df[["Latitude"]].to_numpy())[::-1].astype(
            np.float32
        )
        self.lons = np.squeeze(self.df[["Longitude"]].to_numpy())[::-1].astype(
            np.float32
        )
        self.intensities = np.squeeze(self.df[["Intensity"]].to_numpy())[::-1].astype(
            np.float32
        )
        print(self.times.shape)
        dt = parse_colorado_state_time(
            dateTimeStr=self.times[-1]
        ) - parse_colorado_state_time(dateTimeStr=self.times[0])
        print("DT: ", dt.total_seconds())
        self.seconds = []  # seconds from the start of the storm
        for time in self.times:
            self.seconds.append(
                (
                    parse_colorado_state_time(dateTimeStr=time)
                    - parse_colorado_state_time(dateTimeStr=self.times[0])
                ).total_seconds()
            )
        self.seconds = np.array(self.seconds).astype(np.float32)
        print("seconds:", self.seconds)
        # for interpolation, set longitudes to be a continuous variable

        self.intensityf = scipy.interpolate.interp1d(
            self.seconds, self.intensities, kind="cubic"
        )
        self.latf = scipy.interpolate.interp1d(self.seconds, self.lats, kind="cubic")
        self.lons = kill_day_border(self.lons)
        self.lonf = scipy.interpolate.interp1d(self.seconds, self.lons, kind="cubic")
        self.lons = awake_day_border(self.lons)

    def get_intensity(self, dateTime=None):
        """
        colorado state
        1d interpolate windspeed, see
        https://docs.scipy.org/doc/numpy/reference/generated/numpy.interp.html
        """
        seconds = (
            dateTime - parse_colorado_state_time(dateTimeStr=self.times[0])
        ).total_seconds()
        return float(self.intensityf(seconds))

    def get_interpolated_lat_lon(self, dateTime=None):
        """
        colorado state
        """
        seconds = (
            dateTime - parse_colorado_state_time(dateTimeStr=self.times[0])
        ).total_seconds()
        return (
            float(self.latf(seconds)),
            awake_day_border([float(self.lonf(seconds))])[0],
        )
