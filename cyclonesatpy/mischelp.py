"""
routines that did not fit to any category...
"""

import argparse
import os
import numpy as np
from pathlib import Path
import matplotlib as mpl
import matplotlib.pyplot as plt


def str2bool(v):
    """
    get boolean args to argparse
    https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse

    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def replace_nans(a):
    """ replace nans with median """
    median = np.nanmedian(a)
    print("median:", median)
    np.nan_to_num(a, nan=median, posinf=median, neginf=median)
    return a


def clean_local_dir_except_files_ending(localDir=None, keepEndings=[]):
    """
    from a directory 'localDir'
    remove all files except those that end as in 'keepEndings' list
    """
    allFiles = os.listdir(localDir)
    keepFiles = []
    for ke in keepEndings:
        keepFiles = keepFiles + [k for k in allFiles if k.endswith(ke)]

    deleteFiles = [item for item in allFiles if item not in keepFiles]
    # print(f"keepFiles {keepFiles}")

    # print(f"delFiles {deleteFiles}")
    # sys.exit()
    for filename in deleteFiles:
        try:
            # print(f"kill file: {os.path.join(localDir, filename)}")
            Path(os.path.join(localDir, filename)).unlink()
        except:
            pass


def windspeed_to_category(windspeed):
    """
    Converts windspeed to a storm category.

    Args:
        windspeed (float): The windspeed in knots.

    Raises:
        ValueError: If windspeed is out of bounds.

    Returns:
        int: The storm category.
    """

    assert windspeed >= 0, f"Windspeed must be >= 0, got {windspeed}"

    if windspeed < 21:
        hclass = 1  # NC No Category
    elif windspeed < 34:
        hclass = 2  # TD Tropical Depression
    elif windspeed < 64:
        hclass = 3  # TS Tropical Storm
    elif windspeed < 83:
        hclass = 4  # H1 Class 1
    elif windspeed < 96:
        hclass = 5  # H2 Class 2
    elif windspeed < 113:
        hclass = 6  # H3 Class 3
    elif windspeed < 137:
        hclass = 7  # H4 Class 4
    elif windspeed <= 200:
        hclass = 8  # H5 Class 5
    else:
        raise ValueError(f"Windspeed must be <= 200, got {windspeed}")

    return hclass


def change_yaml_line(filename, min_value, max_value):
    """ change yaml file, update parameters from new_dict"""
    with open(filename, "r") as in_file:
        lines = in_file.readlines()

    lines[-1] = (
        "            - {colors: greys, min_value: "
        + str(min_value)
        + ", max_value: "
        + str(max_value)
        + "}"
        + "\n"
    )

    with open(filename, "w") as out_file:
        out_file.writelines(lines)

def str2bool(v):
    """
    get boolean args to argparse
    https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def plot_rain_rate_image(precip, name, plot=True):
    """ plot data with defined color scale """
    boundaries = [0, 0.1, 0.2, 0.3, 0.5, 1, 2., 3, 5, 10, 15, 20, 30, 40, 50, 100]
    cmap = mpl.colors.ListedColormap([
        "#c0c0c0",
        "#708090",
        "#010081",
        "#0000fe",
        "#4169e2",
        "#00ffff",
        "#32cd33",
        "#008001",
        "#fffe02",
        "#fed700",
        "#ffa07a",
        "#ff7f50",
        "#fe0000",
        "#800000",
        "#000000",
    ])
    norm = mpl.colors.BoundaryNorm(boundaries, cmap.N, clip=True)
    if plot:
        fig = plt.figure(figsize=(16, 12))
        ctr = plt.imshow(precip, cmap=cmap, norm=norm)
        cbar = plt.colorbar(ctr)
        cbar.set_ticks(boundaries)
        fig.patch.set_facecolor("white")
        fig.savefig(name, dpi=300)
    return boundaries, cmap, norm