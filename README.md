# Generate ir images as jpg files for desired storm 

- Uses satpy, and that is why this repo has GPL license 
https://github.com/pytroll/satpy

- At the moment only infrared channel implemented (C13 in goes, B13 in himawari8)

# Install
```
cd ~/git/cyclonesatpy/cyclonesatpy
mkdir ~/venvs
python3 -m venv ~/venvs/devSatpy
source ~/venvs/devSatpy/bin/activate
pip install -r requirements.txt
pip install -e .  # to install package cyclonesatpy
``` 

# Usage 

- env
```
export PYTHONPATH="$HOME/git/cyclonesatpy/"
```

- download ine storm (hHaishen 2020), make images every 12h (==720 minutes)

``` 
python3 satpyCycloneToAws.py --stormId wp112020 --dMinutes 720 --xsize 1600 --ysize 1600 --channel ir --resolution 2.0
```

- download a selection of storms (selection hardcoded)

``` 
python3 manyCyclonesToAwsParallel.py
``` 

